import javafx.application.Application;

/**
 * I'm only adding this class so that I can debug JavaFX in VS Code.
 */
public class MapperApplication {
    public static void main(String[] args) {
        Application.launch(Mapper.class);
    }
}