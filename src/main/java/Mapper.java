import java.io.IOException;

import com.esri.arcgisruntime.mapping.view.MapView;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Main JavaFX application with all UI elements.
 *
 * FYI: JavaFx does screen scaling (e.g., 125% on Windows). This impacts
 * images/sizing (e.g., makes mapView bigger than expected).
 */
public class Mapper extends Application {

    private MapView mapView;

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("layout.fxml"));
        Scene scene = new Scene(root);

        stage.setTitle("Mapper");
        stage.setWidth(600);
        stage.setHeight(400);
        stage.setScene(scene);
        stage.getIcons().add(new Image(Mapper.class.getResourceAsStream("icon.png")));
        stage.show();

        // TODO Need to do:
        // fix build artifact on Linux (i.e., include windows jars)
        // add versioning and build metadata

        // TODO Maybe do:
        // can a modal or loading graphic be used during async events
        // restructure code in clearer fashion if possible
        // research using xml for layout

        // TODO probably won't do:
        // modify styles based on size
    }

    /**
     * Stops and releases all resources used in application.
     */
    @Override
    public void stop() {
        if (mapView != null) {
            mapView.dispose();
        }
    }

}