import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import com.esri.arcgisruntime.UnitSystem;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.layers.KmlLayer;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.Basemap.Type;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.ogc.kml.KmlDataset;
import com.esri.arcgisruntime.toolkit.Scalebar;

import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class MapperController {
    @FXML
    private StackPane temporaryStackPane;
    @FXML
    private TextField txtWidth;
    @FXML
    private TextField txtHeight;
    @FXML
    private TextField txtDensity;
    @FXML
    private TextField txtPadding;
    @FXML
    private TextField txtKMLPath;
    @FXML
    private ChoiceBox<Type> ddlBaseMap;
    @FXML
    private MapView mapView;
    @FXML
    private Scalebar scalebar;
    private FileChooser fileChooser;
    private ArcGISMap map;
    private KmlLayer kmlLayer;
    private String kmlFilename;

    public MapperController() {
        super();
        map = new ArcGISMap(Basemap.createTopographic());
        System.out.println("Created map");

        fileChooser = new FileChooser();
        fileChooser.setTitle("Select KML file");
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("KML Files", "*.kml"));
    }

    @FXML
    public void initialize() {
        mapView.setMap(map);
        System.out.println("setMap");

        mapView.addDrawStatusChangedListener((l) -> {
            System.out.println("MAP View status: " + l.getDrawStatus());
        });

        ddlBaseMap.setItems(FXCollections.observableArrayList(Basemap.Type.values()));
        ddlBaseMap.setValue(Type.TOPOGRAPHIC); // Would be nice if ARCGIS added map.getBasemap().getType()

        // TODO temporary until scalebar works in FXML
        scalebar = new Scalebar(mapView, Scalebar.SkinStyle.GRADUATED_LINE);
        scalebar.setUnitSystem(UnitSystem.IMPERIAL);
        StackPane.setAlignment(scalebar, Pos.BOTTOM_LEFT);
        scalebar.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        temporaryStackPane.getChildren().add(scalebar);
    }

    @FXML
    protected void handle_btnPickKMLFile_OnAction(ActionEvent event) {
        File selectedFile = fileChooser.showOpenDialog((Stage) ddlBaseMap.getScene().getWindow());
        if (selectedFile != null) {
            txtKMLPath.setText(selectedFile.getAbsolutePath());
        }
    }

    @FXML
    protected void handle_btnLoad_OnAction(ActionEvent event) {
        Double width = Double.parseDouble(txtWidth.getText());
        Double height = Double.parseDouble(txtHeight.getText());
        Double density = Double.parseDouble(txtDensity.getText());
        Double padding = Double.parseDouble(txtPadding.getText());

        // Set mapview size without padding
        mapView.setPrefSize((width - padding * 2.0) * density, (height - padding * 2.0) * density);

        // Set basemap
        switch (Basemap.Type.values()[ddlBaseMap.getSelectionModel().getSelectedIndex()]) {
            case DARK_GRAY_CANVAS_VECTOR: {
                map.setBasemap(Basemap.createDarkGrayCanvasVector());
                break;
            }
            case IMAGERY: {
                map.setBasemap(Basemap.createImagery());
                break;
            }
            case IMAGERY_WITH_LABELS: {
                map.setBasemap(Basemap.createImageryWithLabels());
                break;
            }
            case IMAGERY_WITH_LABELS_VECTOR: {
                map.setBasemap(Basemap.createImageryWithLabelsVector());
                break;
            }
            case LIGHT_GRAY_CANVAS: {
                map.setBasemap(Basemap.createLightGrayCanvas());
                break;
            }
            case LIGHT_GRAY_CANVAS_VECTOR: {
                map.setBasemap(Basemap.createLightGrayCanvasVector());
                break;
            }
            case NATIONAL_GEOGRAPHIC: {
                map.setBasemap(Basemap.createNationalGeographic());
                break;
            }
            case NAVIGATION_VECTOR: {
                map.setBasemap(Basemap.createNavigationVector());
                break;
            }
            case OCEANS: {
                map.setBasemap(Basemap.createOceans());
                break;
            }
            case OPEN_STREET_MAP: {
                map.setBasemap(Basemap.createOpenStreetMap());
                break;
            }
            case STREETS: {
                map.setBasemap(Basemap.createStreets());
                break;
            }
            case STREETS_NIGHT_VECTOR: {
                map.setBasemap(Basemap.createStreetsNightVector());
                break;
            }
            case STREETS_VECTOR: {
                map.setBasemap(Basemap.createStreetsVector());
                break;
            }
            case STREETS_WITH_RELIEF_VECTOR: {
                map.setBasemap(Basemap.createStreetsWithReliefVector());
                break;
            }
            case TERRAIN_WITH_LABELS: {
                map.setBasemap(Basemap.createTerrainWithLabels());
                break;
            }
            case TERRAIN_WITH_LABELS_VECTOR: {
                map.setBasemap(Basemap.createTerrainWithLabelsVector());
                break;
            }
            case TOPOGRAPHIC: {
                map.setBasemap(Basemap.createTopographic());
                break;
            }
            case TOPOGRAPHIC_VECTOR: {
                map.setBasemap(Basemap.createTopographicVector());
                break;
            }
            default:
                break;
        }

        kmlFilename = new File(txtKMLPath.getText()).getName();

        // Load KML
        KmlDataset fileKmlDataset = new KmlDataset(txtKMLPath.getText());
        kmlLayer = new KmlLayer(fileKmlDataset);
        kmlLayer.addDoneLoadingListener(() -> {
            System.out.println("KML Layer done loading");
            // Zoom to KML
            mapView.setViewpoint(new Viewpoint(kmlLayer.getFullExtent()));
            /**
             * TODO We have a race condition In the app, change padding and click load,
             * eventually padding doesn't get set correctly. Need to check both
             * mapView.setPrefSize calls.
             */
            // Set mapview size to include padding
            mapView.setPrefSize(width * density, height * density);
        });
        map.getOperationalLayers().clear();
        map.getOperationalLayers().add(kmlLayer);
    }

    @FXML
    protected void handle_btnSaveImage_OnAction(ActionEvent event) {
        ListenableFuture<Image> mapImage = mapView.exportImageAsync();
        mapImage.addDoneListener(() -> {
            System.out.println("MAP Image done");
            try {
                Image image = mapImage.get();

                var shp = new SnapshotParameters();
                WritableImage wi = new WritableImage((int) scalebar.getWidth(), (int) scalebar.getHeight());
                wi = scalebar.snapshot(shp, wi);
                Image scalebarImage = (Image) wi;

                FileChooser fcOutputImage = new FileChooser();
                fcOutputImage.setInitialDirectory(new File("."));
                fcOutputImage.setInitialFileName(kmlFilename.replaceFirst("\\.[kK][mM][lL]$", "") + '-'
                        + Basemap.Type.values()[ddlBaseMap.getSelectionModel().getSelectedIndex()] + '-' + 'h'
                        + txtHeight.getText() + '-' + 'w' + txtWidth.getText() + '-' + 'p' + txtPadding.getText() + '-'
                        + 'd' + txtDensity.getText() + ".png");

                File file = fcOutputImage.showSaveDialog((Stage) ddlBaseMap.getScene().getWindow());
                if (file != null) {
                    // load source images
                    BufferedImage biMap = SwingFXUtils.fromFXImage(image, null);
                    BufferedImage biScalebar = SwingFXUtils.fromFXImage(scalebarImage, null);

                    BufferedImage combined = new BufferedImage(biMap.getWidth(), biMap.getHeight(),
                            BufferedImage.TYPE_INT_ARGB);

                    // paint both images, preserving the alpha channels
                    Graphics gfx = combined.getGraphics();
                    gfx.drawImage(biMap, 0, 0, null);
                    gfx.drawImage(biScalebar, 0, biMap.getHeight() - biScalebar.getHeight(), null);

                    gfx.dispose();

                    // Save as new image
                    ImageIO.write(combined, "png", file);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }
}
